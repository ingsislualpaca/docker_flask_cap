#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 29 11:23:52 2021

@author: orc
"""

import pandas as pd
from data_s import Data

df  = pd.read_csv("iris.csv")

data_p = Data(df=df)

max_f, min_f, sd_f, mean_f = data_p.resum_data()

data_p.hist_data()
